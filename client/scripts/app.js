(function() {
    'use strict';

    angular
        .module('laCandela', [
            'ngRoute',
            'laCandela.main'
        ])

    .config(function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: './views/main.html',
                controller: 'mainController'
            })

        .otherwise({
            redirectTo: '/'
        });
    });

}());

/**
 * @ngdoc overview
 * @name testyoangularApp
 * @description
 * # testyoangularApp
 *
 * Main module of the application.

angular
  .module('testyoangularApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/contact', {
        templateUrl: 'views/contact.html',
        controller: 'ContactCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  }); 

  */
