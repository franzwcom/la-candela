(function() {

    angular
        .module("laCandela.main", [])
        .controller("mainController", mainController);

    function mainController($scope) {

        $scope.upcomingParties = [{
            "day": 19,
            "month": "June"
        }, {
            "day": 3,
            "month": "July"
        }, {
            "day": 7,
            "month": "August"
        }, {
            "day": 28,
            "month": "August"
        }, {
            "day": 2,
            "month": "October"
        }, {
            "day": 6,
            "month": "November"
        }, {
            "day": 4,
            "month": "December"
        }];

        $scope.godjsalsani = function() {
        $location.url('http://djsalsani.com');
    };


    }

}());
