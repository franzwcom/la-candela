/*
|-----------------------------------------------------------
| gulps 
|-----------------------------------------------------------
*/

var concat = require('gulp-concat');
var uglify = require('gulp-uglify'); 
var watch = require('gulp-watch'); 
var minifyCSS = require('gulp-minify-css');
var sass = require('gulp-sass');  
var autoprefixer = require('gulp-autoprefixer'); 
var notify = require('gulp-notify'); 

var size = require('gulp-filesize'); // gets size file
var gutil = require('gulp-util'); // for errors .on('error', gutil.log)
var rename = require('gulp-rename');  
var jshint = require('gulp-jshint');  


//var bowerSrc = require('gulp-bower-src');

var livereload = require('gulp-livereload'); 

// minify html
var minifyHTML = require('gulp-minify-html'); 

// Add angularjs dependency injection annotations with ng-annotate
var ngAnnotate = require('gulp-ng-annotate');


var gulp = require('gulp'); 
var browserSync = require('browser-sync').create(); /// http://www.browsersync.io/docs/gulp/
var reload = browserSync.reload;


/*
|-----------------------------------------------------------
| paths 
|-----------------------------------------------------------
*/




// Static server
gulp.task('browser-sync', function() {
    browserSync.init({
        server: "./app"
    });
});


/*
 * Scss files
 */

var sassPath = 'client/styles/main.scss';
gulp.task('sassies', function() {
    return gulp.src(sassPath)
        .pipe(sass({
            errLogToConsole: true
        }))
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        //.pipe(gulp.dest('dist/css'))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(minifyCSS({
            keepBreaks: true, // line by line or mashed up : false
            keepSpecialComments: 1
        }))
        .pipe(gulp.dest('./app/public/css'))
        .pipe(size())
        //.pipe(livereload()) // reloads page
        .pipe(notify({
            message: 'Sass task complete'
        }))
        .pipe(reload({
            stream: true
        }));
});


/*
 * htmls
 */
var pathHtml = 'client/**/*.html';

gulp.task('minify-html', function() {
    var opts = {
        conditionals: true,
        spare: true,

    };

    return gulp.src(pathHtml)
        .pipe(minifyHTML(opts))
        .pipe(gulp.dest('./app'))
        .pipe(size())
        //.pipe(livereload());
        .pipe(reload({
            stream: true
        })); //livereload !!
});
/*_________________________________________*/

/*
|-----------------------------------------------------------
| watch changes 
|-----------------------------------------------------------
*/
gulp.task('watch', function() {
    gulp.watch('client/styles/main.scss', ['sassies']);
    gulp.watch('client/**/*.html', ['minify-html']);
    gulp.watch('client/scripts/**/*.js', ['scripts']);
    gulp.watch(bowers, ['lebowers']);
});



/*
 * the scripts
 */
 var pathScripts = 'client/scripts/**/*.js';

gulp.task('scripts', function() {
    return gulp.src(pathScripts)
        .pipe(ngAnnotate())
        .pipe(jshint('.jshintrc')) // options here : http://jshint.com/docs/options/
        .pipe(jshint.reporter('default'))
        .pipe(concat('master.js'))
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('./app/public/js'))
        .on('error', gutil.log)
        .pipe(size())
        // .pipe(livereload())
        .pipe(notify({
            message: 'js task complete'
        }))
        .pipe(reload({
            stream: true
        }));
});

/*
 * bowers
 */
var bowers = [
    "bower_components/jquery/dist/jquery.min.js",
    "bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js",
    "bower_components/angular/angular.min.js",
    "bower_components/angular-route/angular-route.min.js",
    "bower_components/wow/dist/wow.min.js"
];

gulp.task('lebowers', function() {
    return gulp.src(bowers)
        .pipe(concat('vendors.js'))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('./app/public/js'))
        .pipe(size())
        //.pipe(livereload())
        .pipe(notify({
            message: 'le bowers task complete'
        }))
        .pipe(reload({
            stream: true
        }));
});



/*
|-----------------------------------------------------------
| extras
| http://jsfiddle.net/xX4j4/ 
| https://github.com/pwnjack/yeoman-gulp-less/blob/master/gulpfile.js
|-----------------------------------------------------------
*/


// Static Server + watching scss/html files
gulp.task('serve', /*['styles'],*/ function() {

    browserSync.init({
        server: "./app"
    });

    gulp.watch('client/styles/main.scss', ['sassies']);
    gulp.watch('client/**/*.html', ['minify-html']);
    gulp.watch('client/scripts/**/*.js', ['scripts']);
    gulp.watch(bowers, ['lebowers']);

});
gulp.task('default', ['serve']);



// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src("app/scss/*.scss")
        .pipe(sass())
        .pipe(gulp.dest("app/css"))
        .pipe(reload({
            stream: true
        }));
});
